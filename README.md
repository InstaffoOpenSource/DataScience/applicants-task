# Intro

For your next step in the application process at Instaffo we'd like you to do the tasks given below to be able to further assess your skills and knowledge. Please send a link to a publicly accessible git repository containing your solution to the person managing your application process.

The end result of your submitted solution is not all that counts. Code quality (including project structure), dependencies and environment management, documentation (docstrings, comments, README file, etc.) are **equally** important!

We wish you good luck (and also a lot of fun) with the tasks! 🍀

# Job Title Classification

When filling out their profile on our platform, candidates enter their current job title and the corresponding category (e.g. "IT", "Sales" or "Marketing and Communication"). To improve the user experience we want to suggest the correct category based on the job title entered by the candidate. This way candidates don't need to manually select the category anymore, provided that the accuracy of the suggested category is high enough.

## Task 1

For the above mentioned Machine Learning project we first need to clean and prepare the provided data. The goal is to prepare it in such a way that you can directly start extracting features (if necessary) and training a model in the second task (see _**Task 2**_ below).

The data (see _**data.json**_) provided has the following structure:

```json
[
    {
        "job_title": ...,
        "category": ...
    },
    ...
]
```

Your task is to implement a pipeline in Python which loads the raw data, cleans/transforms and exports it. Some additional information which may be useful for you:

- Inspect the raw data and check for yourself what cleaning steps are necessary and/or beneficial.
- You're free to choose the output format & structure of the cleaned data. The goal is to make it as easy and fast as possible for the second task (see _**Task 2**_ below) to load and work with the data you provide for feature extraction (if necessary) and training a model.
- A jupyter notebook is only allowed for inspecting the data but **not** for the pipeline itself.

## Task 2

Before starting to implement a Machine Learning project it's usually wise to do research and come up with possible solutions that should/may/could work.

Your task is to describe an approach in detail which you think should yield an accuracy high enough for a good user experience. If you can think of multiple approaches you're free to write down more than one but it's **not** necessary. **No implementation or any kind of code is required in this task!**

Some important requirements:

- Out of vocabulary job titles (= job titles not seen in the training data) need to be supported. Typos happen (e.g. _"data scintist"_) and the model should be able to still output a meaningful category with a high confidence score.
- Since this model will be used in a real time scenario the prediction time shouldn't be too high.
- The model size shouldn't be too big. We want to be cost effective and don't spend too much money on servers.
- We don't want to reinvent the wheel. Use existing & proven libraries/frameworks for your solution if possible.

Your description, among other things, should include the following information:

- What tools/libraries/frameworks are used.
- Links to papers if appropriate.
- What specific algorithm, neural network, etc. is used.
- How the data needs to be transformed for training (e.g. feature extraction if necessary). Cleaning steps are not necessary since this has all been done in _**Task 1**_.
- Advantages & disadvantages as well as possible risks of your approach.
- What metrics can be used to measure performance of the trained model.

No long essay is needed. Try to keep it short but be specific. An imaginary fellow colleague should be able to implement your approach with the description given without having the need to ask any questions (e.g. because important details are missing). And yes, the imaginary fellow colleague is a Python god and knows all existing tools/frameworks/libraries, so no need to explain how to e.g. train a SVM or do hyper parameter tuning 😉
